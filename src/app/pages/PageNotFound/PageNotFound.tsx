import React, { FC } from "react";
import { NavLink } from "react-router-dom";

import brokenLampIcon from "../../../assets/icons/broken-lamp.png"
import arrowNextIcon from "../../../assets/icons/arrow-next.svg"

import "./PageNotFound.scss";

const PageNotFound: FC = () => {

    return (
        <div className="page--404">
            <div className="page--404__img">
                <img
                    src={brokenLampIcon}
                    alt="#"
                    className="page--404__img--item"
                />
            </div>
            <div className="page--404__info">
                <h1 className="page--404__info--title">404</h1>
                <p className="page--404__info--text">The page you are looking for not available...</p>
                <NavLink to="/" className="link">
                    Go to Home
                    <img src={arrowNextIcon} alt="#" className="page--404__info--arrow" />
                </NavLink>
            </div>
        </div>
    )
}

export default PageNotFound
