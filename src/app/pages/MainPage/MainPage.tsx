import React, { FC, useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";

import Info from "../../../client/modules/Info";
import Users from "../../../client/modules/Users";
import RegisterUser from "../../../client/modules/RegisterUser";
import Modal from "../../../shared/UIComponents/Modal";
import { RootStore } from "../../../store/store";
import { getTokenAction } from "../../../store/actions/getTokenAction";


const MainPage: FC = () => {
    const { modal } = useSelector(({ modal }: RootStore) => modal, shallowEqual)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getTokenAction())
    }, [dispatch])
    return (
        <div className="main">
            <Info />
            <Users />
            <RegisterUser />
            {modal.isOpen && <Modal />}
        </div>
    )
}

export default MainPage
