import React, { FC } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from "react-redux";

import { AppContext } from "../../context/AppContext";

import Header from "../../../client/modules/Header";
import RoutesComponent from "../RoutesComponent";
import Footer from "../../../client/modules/Footer";

import { store } from "../../../store/store";
import './AppComponent.scss';

const AppComponent: FC<any> = () => {
  return (
      <AppContext>
          <Provider store={store}>
              <Router>
                <div className="App">
                    <Header />
                    <RoutesComponent />
                    <Footer />
                </div>
              </Router>
          </Provider>
      </AppContext>
  );
}

export default AppComponent;
