import React, { FC } from "react";
import { Field, FormikFormProps } from "formik";

import { IPosition } from "../../../store/reducers/positionReducer";
import "./FormSelector.scss";

interface IFormSelectorProps extends FormikFormProps {
    label: string,
    list: IPosition[],
    handlePosition: (position: string) => void,
    position: string
}

const FormSelector: FC<IFormSelectorProps> = (props) => {
    const { label, list, name, handlePosition, position } = props
    return (
        <label htmlFor={label} className="form--label">
            {label}
            <div className="form--label__list">
                {list.map((item, idx) => {
                    return (
                        <div key={idx} className='form--label__list--item'>
                            <Field
                                className={'form--label__list--item--field'}
                                type='radio'
                                name={name}
                                value={item}
                                checked={item.name === position || item.id === 1}
                            />
                            <span
                                className="form--label__list--item--text"
                                onClick={() => handlePosition(item.name)}
                            >{item.name}</span>
                        </div>
                    )
                })}
            </div>
        </label>
    )
}

export default FormSelector
