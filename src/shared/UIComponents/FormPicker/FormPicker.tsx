import React, { FC, useRef } from "react";

import "./FormPicker.scss";

interface IFormPickerProps {
    label: string,
    name: string,
    handleFile: (file: File) => void,
    file: any,
    errorMessage?: string,
    fieldTouched?: boolean,
    setFieldTouched?: (field: string, value: any, shouldValidate?: boolean) => void
}

const FormPicker: FC<IFormPickerProps> = (props) => {
    const { label, name, handleFile, file, fieldTouched, errorMessage, setFieldTouched } = props
    // const errorClass = (errorMessage && fieldTouched) && 'error'
    const errorMessageStyle = (errorMessage && fieldTouched) ? {
        transition: '.4s',
        opacity: 1
    } : {
        opacity: 0
    }

    const fileInput = useRef<HTMLInputElement>(null)
    const handleClick = (e: any) => {
        e.preventDefault()
        if (fileInput.current !== null) {
            fileInput.current.click();
        }
    }

    const handleChange = (event: any) => {
        const fileUploaded = event.target.files[0];
        setFieldTouched && setFieldTouched(name, !!event.target.files[0].name.length)
        handleFile(fileUploaded)
    };

    return (
        <label className="form--picker">
            {label}
            <div className="form--picker__field">
                <input
                    name={name}
                    ref={fileInput}
                    className={"form--picker__field--path"}
                    type='file'
                    style={{ display: 'none' }}
                    onChange={handleChange}
                />
                <span className="form--picker__field--path">{file?.name || 'Upload your photo'}</span>
                <button
                    type='button'
                    className="form--picker__field--btn btn"
                    onClick={handleClick}
                >Browse</button>
            </div>
            <span
                className="error--message"
                style={errorMessageStyle}
            >{errorMessage}</span>
        </label>
    )
}

export default FormPicker
