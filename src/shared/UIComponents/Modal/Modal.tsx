import React, { FC } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";

import { RootStore } from "../../../store/store";
import { ModalConstants } from "../../../store/reducers/modalReducer";
import "./Modal.scss";

const Modal: FC<any> = () => {
    const { modal } = useSelector(({ modal }: RootStore) => modal, shallowEqual)
    const dispatch = useDispatch()
    const onClose = () => dispatch({ type: ModalConstants.CLOSE_MODAL })
    return (
        <div className="modal__overlay">
            <div className="modal">
                <div className="modal__head">
                    <h2 className="modal__head--title heading-2">{modal.success ? "Congratulations" : "Fails"}</h2>
                    <div className="modal__head__close" onClick={onClose}>
                        <span className="modal__head__close--item" />
                        <span className="modal__head__close--item" />
                    </div>
                </div>
                <div className="modal__body">
                    <h3 className="modal__body--title">{modal.message}</h3>
                </div>
                <div className="modal__footer">
                    <button
                        type="button"
                        className="modal__footer--btn btn"
                        onClick={onClose}
                    >Great</button>
                </div>
            </div>
        </div>
    )
}

export default Modal
