import React, { FC, useEffect } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { Form, Formik, FormikValues } from "formik";
import * as Yup from 'yup';
import { scrollMore } from "react-scroll/modules/mixins/animate-scroll";

import FormField from "../../../shared/UIComponents/FormField";
import FormSelector from "../../../shared/UIComponents/FormSelector";
import FormPicker from "../../../shared/UIComponents/FormPicker";
import Loader from "../../../shared/UIComponents/Loader";
import { RootStore } from "../../../store/store";
import { addUserAction } from "../../../store/actions/addUserAction";
import { getUsersAction } from "../../../store/actions/getUsersAction";
import { getPositionAction } from "../../../store/actions/getPositionAction";
import "./RegisterUser.scss";

const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/gif",
    "image/png"
];

const SignupSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(15, 'Too Long!')
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    phone: Yup.string()
        .required('Required')
        .matches(/^[+]{0,1}38([0-9, (,)]{5})([0-9, ]{4})([0-9, ]{3})([0-9, ]{3})$/g, 'Phone number must be a 12 characters'),
    position: Yup.string().required('Required'),
    photo: Yup.mixed()
        .required("A file is required")
        .test(
            "fileFormat",
            "Unsupported Format",
            value => value && SUPPORTED_FORMATS.includes(value.type)
        )
});

const RegisterUser: FC<any> = () => {
    const { token } = useSelector(({ modal }: RootStore) => modal, shallowEqual)
    const { positions } = useSelector(({ userPosition }: RootStore) => userPosition, shallowEqual)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getPositionAction())
    }, [dispatch])

    const onSubmit = (values: FormikValues, { resetForm }: any): void => {
        const formData = new FormData()
        formData.append("name", values.name)
        formData.append("email", values.email)
        formData.append("phone", values.phone.replace(/[(,), ]/g, ''))
        formData.append("position", values.position)
        formData.append("position_id", '2')
        formData.append("photo", values.photo)
        token && dispatch(addUserAction(formData, token))
        dispatch(getUsersAction(1, window.innerWidth >= 540 ? 6 : 3, true))
        resetForm()
    }
    return (
        <div className="register--user" id='sign-up'>
            <div className="container">
                <h2 className="register--user--title heading-1">Register to get a work</h2>
                <h3 className="register--user--subtitle heading-2">Attention! After successful registration and alert, update the list of users in the block from the top</h3>
                <Formik
                    initialValues={{
                        name: '',
                        email: '',
                        phone: '',
                        position: positions[0]?.name,
                        photo: ''
                    }}
                    onSubmit={onSubmit}
                    validationSchema={SignupSchema}
                >
                    {({
                          setFieldValue,
                          values,
                          errors,
                          touched,
                          setFieldTouched,
                          handleChange,
                          resetForm
                      }) => {
                        // console.log(values, errors, touched)
                        return (
                            <Form className="register--user__form">
                                <FormField
                                    label={'Name'}
                                    name={'name'}
                                    placeholder={'Your name'}
                                    errorMessage={errors.name}
                                    fieldTouched={touched?.name}
                                />
                                <FormField
                                    label={'Email'}
                                    name={'email'}
                                    placeholder={'Your email'}
                                    errorMessage={errors.email}
                                    fieldTouched={touched?.email}
                                />
                                <FormField
                                    label={'Phone number'}
                                    name={'phone'}
                                    mask={"+99(999) 999 99 99"}
                                    message={'Enter a phone number in international format'}
                                    handleChange={handleChange}
                                    setFieldTouched={setFieldTouched}
                                    errorMessage={errors.phone}
                                    fieldTouched={touched?.phone}
                                />
                                <FormSelector
                                    label={'Select your position'}
                                    name={'position'}
                                    list={positions}
                                    handlePosition={position => setFieldValue('position', position)}
                                    position={values.position}
                                />
                                <FormPicker
                                    label={'Photo'}
                                    name={'photo'}
                                    handleFile={file => setFieldValue('photo', file)}
                                    file={values.photo}
                                    errorMessage={errors.photo}
                                    fieldTouched={touched.photo}
                                    setFieldTouched={setFieldTouched}
                                />
                                <Loader />
                                <button
                                    style={{ marginTop: '40px' }}
                                    type="submit"
                                    className="btn"
                                    onSubmit={() => {
                                        scrollMore(1500)
                                        onSubmit(values, { resetForm })
                                    }}
                                >
                                    Sign up now
                                </button>
                            </Form>
                        )
                    }}
                </Formik>
            </div>
        </div>
    )
}

export default RegisterUser
