import React, { FC, useState } from "react";

import Navbar from "../../components/Navbar";
import HeaderInfo from "../../components/HeaderInfo";
import Sidebar from "../../components/Sidebar";

const Header: FC = () => {
    const [showSidebar, setShowSidebar] = useState<boolean>(false)
    const toggleVisibleSidebar = (visible?: boolean) => {
        setShowSidebar(visible || false)
    }
    return (
        <header className="header">
            <Navbar
                toggleVisibleSidebar={toggleVisibleSidebar}
                showSidebar={showSidebar}
            />
            <HeaderInfo />
            <Sidebar
                toggleVisibleSidebar={toggleVisibleSidebar}
                showSidebar={showSidebar}
            />
        </header>
    )
}

export default Header
