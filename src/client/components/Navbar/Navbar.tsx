import React, { FC } from "react";
import { NavLink } from "react-router-dom";

import LinkItem from "../LinkItem";
import logoText from '../../../assets/img/logo.svg';
import { navbarLinks } from "./navbarLinks";

import "./Navbar.scss"

interface INavbarProps {
    toggleVisibleSidebar: (visible?: boolean) => void,
    showSidebar: boolean
}

const Navbar: FC<INavbarProps> = (props) => {
    const { toggleVisibleSidebar, showSidebar } = props
    return (
        <div className="container">
            <nav className="navbar">
                <NavLink to='/' className="link">
                    <div className="navbar__logo">
                        <img src={logoText} alt="#" className="navbar__logo--img"/>
                    </div>
                </NavLink>
                <div className="navbar__links">
                    <LinkItem
                        links={navbarLinks}
                        className={'navbar__links--item'}
                    />
                </div>
                {!showSidebar && <div
                    className="navbar__burger"
                    onClick={() => toggleVisibleSidebar(true)}
                >
                    <span className="navbar__burger--item"/>
                    <span className="navbar__burger--item"/>
                    <span className="navbar__burger--item"/>
                </div>}
            </nav>
        </div>
    )
}

export default Navbar
