import { navbarLinks } from "../Navbar/navbarLinks";

const firstListLinks = [
    {
        key: 1,
        name: 'How it works',
        to: '/documentation',
        link: true
    },
    {
        key: 2,
        name: 'Partnership',
        to: '/partnership',
        link: true
    },
    {
        key: 3,
        name: 'Help',
        to: '/help',
        link: true
    },
    {
        key: 4,
        name: 'Leave testimonial',
        to: '/leave-testimonial',
        link: true
    },
    {
        key: 5,
        name: 'Contact us',
        to: '/contacts',
        link: true
    }
]

const secondListLinks = [
    {
        key: 1,
        name: 'Articles',
        to: '/articles',
        link: true
    },
    {
        key: 2,
        name: 'Our news',
        to: '/news',
        link: true
    },
    {
        key: 3,
        name: 'Testimonials',
        to: '/testimonials',
        link: true
    },
    {
        key: 4,
        name: 'Licenses',
        to: '/licenses',
        link: true
    },
    {
        key: 5,
        name: 'Privacy Policy',
        to: '/privacy-policy',
        link: true
    }
]

export { firstListLinks, secondListLinks, navbarLinks }

