import React, { FC } from "react";
import { NavLink } from "react-router-dom";

import LinkItem from "../LinkItem";
import logoText from "../../../assets/img/logo.svg";
import { navbarLinks, firstListLinks, secondListLinks } from "./sidebarLinks";

import "./Sidebar.scss";

interface ISidebarProps {
    toggleVisibleSidebar: (visible?: boolean) => void,
    showSidebar: boolean
}

const Sidebar: FC<ISidebarProps> = (props) => {
    const { toggleVisibleSidebar, showSidebar } = props
    const showClass = showSidebar ? "show" : ""
    return (
        <div className={"sidebar__overlay " + showClass} onClick={() => toggleVisibleSidebar()}>
            <nav className={"sidebar " + showClass}>
                <NavLink to='/' className="link">
                    <div className="sidebar__logo">
                        <img src={logoText} alt="#" className="sidebar__logo--img"/>
                    </div>
                </NavLink>
                <div className="sidebar__links">
                    <LinkItem
                        onClick={() => toggleVisibleSidebar(false)}
                        links={navbarLinks}
                        className={'sidebar__links--item'}
                    />
                </div>
                <div className="sidebar__links">
                    <LinkItem
                        onClick={() => toggleVisibleSidebar(false)}
                        links={firstListLinks}
                        className={'sidebar__links--item'}
                    />
                </div>
                <div className="sidebar__links">
                    <LinkItem
                        onClick={() => toggleVisibleSidebar(false)}
                        links={secondListLinks}
                        className={'sidebar__links--item'}
                    />
                </div>
            </nav>
        </div>
    )
}

export default Sidebar
