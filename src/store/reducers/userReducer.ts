export type ResponseData = IStateUser
    | {
    "success": boolean,
    "message": string
}
    | {
    "success": boolean,
    "message": string,
    "fails": {
        "count": string[],
        "page": string[]
    }
}

export enum UserConstants {
    GET_USERS = 'GET_USERS',
    ADD_USER = 'ADD_USER'
}

export interface IUser {
    email: string
    id: number
    name: string
    phone: string
    photo: string
    position: string
    position_id: number
    registration_timestamp: number
}

export interface IStateUser {
    users: IUser[],
    page: number,
    count?: number,
    links?: {
        next_url?: string | null,
        prev_url?: string | null
    },
    success?: boolean
    total_pages?: number
    total_users?: number
}

export type IUserActionsTypes  = {
    type: UserConstants.GET_USERS,
    payload: {
        data: IStateUser,
        clear: boolean
    }
} | {
    type: UserConstants.ADD_USER,
    payload: {
        user: IUser
    }
}

const initialState: IStateUser = {
    users: [],
    page: 1,
    count: 6
}

export const userReducer = (state: IStateUser = initialState, action: IUserActionsTypes) => {
    switch (action.type) {
        case UserConstants.GET_USERS:
            return {
                ...action.payload.data,
                users: (action.payload.clear)
                    ? [...action.payload.data.users]
                    : [...state.users, ...action.payload.data.users],
                page: action.payload.data.page
            }
        case UserConstants.ADD_USER:
            return {
                ...state,
                users: [action.payload.user, ...state.users.filter((item, idx, arr) => idx + 1 !== arr.length)]
            }
        default:
            return state
    }
}
