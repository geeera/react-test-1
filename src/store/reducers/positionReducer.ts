export enum PositionConstants {
    GET_POSITIONS = "GET_POSITIONS"
}

export type IPositionAction = {
    type: PositionConstants.GET_POSITIONS,
    payload: {
        positions: IPosition[]
    }
}

export interface IPosition {
    id: number,
    name: string
}

export interface IPositionState {
    positions: IPosition[] | []
}

const initState: IPositionState = {
    positions: []
}

export const positionReducer = (state: IPositionState = initState, action: IPositionAction) => {
    switch (action.type) {
        case PositionConstants.GET_POSITIONS:
            return {
                positions: [...action.payload.positions]
            }
        default:
            return state
    }
}
