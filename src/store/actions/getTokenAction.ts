import { Dispatch } from "redux";
import { IModalAction, ModalConstants } from "../reducers/modalReducer";
import { axiosConfig } from "../axiosConfig";

export const getTokenAction = () => async (dispatch: Dispatch<IModalAction>) => {
    try {
        const { data } = await axiosConfig.get('/token')
        dispatch({
            type: ModalConstants.GET_TOKEN,
            payload: {
                token: data.token
            }})
    } catch (e) {

    }
}
